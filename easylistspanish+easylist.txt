[Adblock Plus 2.0]
! Title: EasyList Spanish+EasyList
! Last modified: %timestamp%
! Expires: 1 days (update frequency)
! Homepage: https://easylist.to/
!
! EasyList Spanish and EasyList combination subscription
!
%include easylistspanish.txt%
%include easylist.txt%
