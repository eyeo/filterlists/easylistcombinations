[Adblock Plus 2.0]
! Title: Bulgarian list+EasyList
! Last modified: %timestamp%
! Expires: 1 days (update frequency)
! Homepage: https://stanev.org/abp/
!
! Bulgarian list and EasyList combination subscription
!
%include https://stanev.org/abp/adblock_bg.txt%
%include easylist.txt%
