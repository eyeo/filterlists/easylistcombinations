[Adblock Plus 2.0]
! Title: EasyList Italy+EasyList
! Last modified: %timestamp%
! Expires: 1 days (update frequency)
! Homepage: https://easylist.to/
!
! EasyList Italy and EasyList combination subscription
!
%include easylistitaly.txt%
%include easylist.txt%
