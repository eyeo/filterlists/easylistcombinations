[Adblock Plus 1.1]
! Title: EasyList Italy
! Last modified: %timestamp%
! Expires: 1 days (frequenza di aggiornamento)
! Homepage: https://easylist.to/
! Licenza: https://easylist.to/pages/licence.html
!
! EasyList Italy - Complemento italiano per EasyList
!
! Per favore segnala problemi o pubblicità non bloccate
! nel forum: (https://forums.lanik.us/)
! o via e-mail: (easylistitaly@protonmail.com).
!
!----------------Regole generali per le pubblicità da bloccare----------------!
%include easylistitaly:easylistitaly/easylistitaly_general_block.txt%
%include easylistitaly:easylistitaly/easylistitaly_general_block_popup.txt%
!---------------Regole generali per le pubblicità da nascondere---------------!
%include easylistitaly:easylistitaly/easylistitaly_general_hide.txt%
%include easylistitaly:easylistitaly/easylistitaly_allowlist_general_hide.txt%
!---------------------Server pubblicitari di terze parti----------------------!
%include easylistitaly:easylistitaly/easylistitaly_adservers.txt%
%include easylistitaly:easylistitaly/easylistitaly_adservers_popup.txt%
!---------------------Annunci pubblicitari di terze parti---------------------!
%include easylistitaly:easylistitaly/easylistitaly_thirdparty.txt%
%include easylistitaly:easylistitaly/easylistitaly_thirdparty_popup.txt%
!---------------Regole specifiche per le pubblicità da bloccare---------------!
%include easylistitaly:easylistitaly/easylistitaly_specific_block.txt%
%include easylistitaly:easylistitaly/easylistitaly_specific_block_popup.txt%
!--------------Regole specifiche per le pubblicità da nascondere--------------!
%include easylistitaly:easylistitaly/easylistitaly_specific_hide.txt%
!----------------Regole di esclusione per risolvere i problemi----------------!
%include easylistitaly:easylistitaly/easylistitaly_allowlist.txt%
%include easylistitaly:easylistitaly/easylistitaly_allowlist_dimensions.txt%
%include easylistitaly:easylistitaly/easylistitaly_allowlist_popup.txt%
