[Adblock Plus 2.0]
! Title: IndianList+EasyList+mobile-anti-cv
! Last modified: %timestamp%
! Expires: 1 days (update frequency)
! Homepage: https://easylist.to/
!
! IndianList, EasyList and mobile-anti-cv combination subscription
!
%include indianlist.txt%
%include easylist.txt%
%include https://raw.githubusercontent.com/abp-filters/abp-filters-anti-cv/master/mobile-anti-cv.txt%
