[Adblock Plus 2.0]
! Title: EasyList Italy+EasyList (minified)
! Last modified: %timestamp%
! Expires: 1 days (update frequency)
!
%include customfilterlists:easylistitaly-minified.txt%
%include easylist-minified.txt%
