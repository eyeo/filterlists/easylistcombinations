[Adblock Plus 2.0]
! Title: EasyPrivacy+EasyList
! Last modified: %timestamp%
! Homepage: https://easylist.to/
!
! EasyPrivacy and EasyList combination subscription
!
%include easyprivacy.txt%
%include easylist.txt%
