[Adblock Plus 2.0]
! Title: I don't care about cookies
! Last modified: %timestamp%
! Expires: 1 days (update frequency)
! Homepage: https://www.i-dont-care-about-cookies.eu/
!
%include https://hg.adblockplus.org/idcac/raw-file/tip/list.txt%
