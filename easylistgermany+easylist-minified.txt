[Adblock Plus 2.0]
! Title: EasyList Germany+EasyList (minified)
! Last modified: %timestamp%
! Expires: 1 days (update frequency)
!
%include customfilterlists:easylistgermany-minified.txt%
%include easylist-minified.txt%
