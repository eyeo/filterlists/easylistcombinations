[Adblock Plus 2.0]
! Title: Bulgarian list+EasyList+mobile-anti-cv
! Last modified: %timestamp%
! Expires: 1 days (update frequency)
! Homepage: https://stanev.org/abp/
!
! Bulgarian list, EasyList and mobile-anti-cv combination subscription
!
%include https://stanev.org/abp/adblock_bg.txt%
%include easylist.txt%
%include https://raw.githubusercontent.com/abp-filters/abp-filters-anti-cv/master/mobile-anti-cv.txt%
