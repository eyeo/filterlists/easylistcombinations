[Adblock Plus 2.0]
! Title: RuAdList+EasyList (minified)
! Last modified: %timestamp%
! Expires: 1 days (update frequency)
!
%include customfilterlists:ruadlist-minified.txt%
%include easylist-minified.txt%
