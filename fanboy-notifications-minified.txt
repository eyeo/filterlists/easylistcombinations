[Adblock Plus 2.0]
! Title: Fanboy's Notifications Blocking List (minified)
! Last modified: %timestamp%
! Expires: 1 days (update frequency)
!
%include customfilterlists:fanboy-notifications-minified.txt%
