[Adblock Plus 2.0]
! Title: EasyList Lithuania
! Last modified: %timestamp%
! Expires: 1 days (update frequency)
! Homepage: https://github.com/EasyList-Lithuania/easylist_lithuania
!
! EasyList Lithuania subscription
!
%include https://raw.githubusercontent.com/EasyList-Lithuania/easylist_lithuania/master/easylistlithuania.txt%
