[Adblock Plus 2.0]
! Title: Dandelion Sprout's Nordic Filters+EasyList (minified)
! Last modified: %timestamp%
! Expires: 1 days (update frequency)
!
%include customfilterlists:dandelion_sprouts_nordic_filters-minified.txt%
%include easylist-minified.txt%
