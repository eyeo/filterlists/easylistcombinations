[Adblock Plus 2.0]
! Title: EasyList Dutch+EasyList
! Last modified: %timestamp%
! Expires: 1 days (update frequency)
! Homepage: https://easylist.to/
!
! EasyList Dutch and EasyList combination subscription
!
%include easylistdutch:easylistdutch.txt%
%include easylist.txt%
