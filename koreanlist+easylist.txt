[Adblock Plus 2.0]
! Title: KoreanList+EasyList
! Last modified: %timestamp%
! Expires: 1 days (update frequency)
! Homepage: https://forums.lanik.us/viewforum.php?f=111
!
! KoreanList and EasyList combination subscription
!
%include koreanlist.txt%
%include easylist.txt%
