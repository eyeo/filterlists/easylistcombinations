[Adblock Plus 2.0]
! Title: ROList+EasyList
! Last modified: %timestamp%
! Expires: 1 days (update frequency)
! Homepage: https://zoso.ro/rolist/
!
! ROList and EasyList combination subscription
!
%include https://www.zoso.ro/pages/rolist.txt%
%include easylist.txt%
