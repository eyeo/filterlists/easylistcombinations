[Adblock Plus 1.1]
! Title: EasyList Germany ohne Element Hiding-Filter
! Last modified: %timestamp%
! Expires: 1 days (update frequency)
! Homepage: https://easylist.github.io/
! Licence: https://easylist.github.io/pages/licence.html
!
! EasyList Germany - Deutsche Ergänzung zu EasyList
!
! Bitte melde ungeblockte Werbung und fälschlicherweise geblockte Dinge:
! Forum: https://forums.lanik.us/viewforum.php?f=90
! E-Mail: easylist.germany@gmail.com
!
!----------------Allgemeine Regeln zum Blockieren von Werbung-----------------!
%include easylistgermany:easylistgermany/easylistgermany_general_block.txt%
%include easylistgermany:easylistgermany/easylistgermany_general_block_popup.txt%
!---------------------------Third-party Werbeserver---------------------------!
%include easylistgermany:easylistgermany/easylistgermany_adservers.txt%
%include easylistgermany:easylistgermany/easylistgermany_adservers_popup.txt%
!-----------------------------Third-party Werbung-----------------------------!
%include easylistgermany:easylistgermany/easylistgermany_thirdparty.txt%
%include easylistgermany:easylistgermany/easylistgermany_thirdparty_popup.txt%
!-------------Seitenspezifische Regeln zum Blockieren von Werbung-------------!
%include easylistgermany:easylistgermany/easylistgermany_specific_block.txt%
%include easylistgermany:easylistgermany/easylistgermany_specific_block_popup.txt%
!------------------Ausnahmeregeln zum Beheben von Problemen-------------------!
%include easylistgermany:easylistgermany/easylistgermany_allowlist.txt%
%include easylistgermany:easylistgermany/easylistgermany_allowlist_dimensions.txt%
%include easylistgermany:easylistgermany/easylistgermany_allowlist_popup.txt%
