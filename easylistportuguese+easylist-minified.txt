[Adblock Plus 2.0]
! Title: EasyList Portuguese+EasyList (minified)
! Last modified: %timestamp%
! Expires: 1 days (update frequency)
!
%include customfilterlists:easylistportuguese-minified.txt%
%include easylist-minified.txt%
