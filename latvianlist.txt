[Adblock Plus 2.0]
! Title: Latvian List
! Last modified: %timestamp%
! Expires: 1 days (update frequency)
! Homepage: https://forums.lanik.us/viewforum.php?f=99-latvian-list
!
! Latvian List subscription
!
%include https://raw.githubusercontent.com/Latvian-List/adblock-latvian/master/lists/latvian-list.txt%
